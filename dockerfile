FROM python:3

EXPOSE 5003

RUN mkdir /inventory_management
WORKDIR /inventory_management

COPY ./requirements.txt /inventory_management/requirements_inv_db.txt
RUN pip install -r requirements_inv_db.txt

COPY . /inventory_management
CMD ["python", "inventory_management.py"]