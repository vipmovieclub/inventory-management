from flask import Flask
from flask_restful import Api, Resource, reqparse
import mysql.connector


app = Flask(__name__)
api = Api(app)


def connect_db():
    mysql_config = {
        'user': 'root',
        'password': 'root',
        'host': 'inventory_db',
        'database': 'inventory'
    }

    connected = False
    while connected is False:
        try:
            con = mysql.connector.connect(**mysql_config)
            connected = True

        except Exception as e:
            continue

    return con

# This RESTful API will take requests from other microservices and perform operations on the
# inventory db


class CreateItem(Resource):
    def post(self):
        con = connect_db()
        cursor = con.cursor()

        parser = reqparse.RequestParser()
        parser.add_argument('item_name', type=str, help='The name of the item')
        parser.add_argument('item_price', type=int, help='The price of the item')
        parser.add_argument('item_description', type=str, help='The description of the item')
        args = parser.parse_args()

        item_name = args['item_name']
        item_price = args['item_price']
        item_description = args['item_description']

        sql = "INSERT INTO items (item_name, item_price, item_description) VALUES (%s, %s, %s)"

        try:
            cursor.execute(sql, (item_name, item_price, item_description))
            con.commit()
        except Exception as e:
            return {'error': str(e)}


        return {
            'item_name': args['item_name'],
            'item_price': args['item_price'],
            'item_description': args['item_description']
        }





api.add_resource(CreateItem, '/CreateItem')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5003)